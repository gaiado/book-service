<?php

namespace App\Services;

use Google_Client;
use Google_Service_Drive;

class GoogleDriveService {

    public $googleDriveFolder;
    public $service;

    public function __construct(Google_Client $client){
        $this->googleDriveFolder = config('services.google_drive_folder');
        $this->service = new Google_Service_Drive($client);
    }

    public function uploadFile(){
        return $this->performRequest('GET', '/authors');
    }

    public function listFiles(){
        $optParams = array(
            'pageSize' => 10,
            'fields' => 'nextPageToken, files(id, name)'
        );
        $results = $this->service->files->listFiles($optParams);

        return $results->getFiles();
    }
}
