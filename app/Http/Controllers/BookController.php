<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\ApiResponser;
use App\Book;
use App\Services\GoogleDriveService;

class BookController extends Controller
{

    use ApiResponser;

    private $googleDriveService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GoogleDriveService $googleDriveService)
    {
        $this->googleDriveService = $googleDriveService;
    }

    public function index(){
        $books = Book::all();
        return $this->successResponse($books);
    }

    public function test(){
       $files =  $this->googleDriveService->listFiles();
       return $files;
    }

    public function show($book){
        $book = Book::findOrFail($book);
        return $this->successResponse($book);
    }

    public function store(Request $request){
        $rules = [
            'name' => 'required|max:255',
            'gender' => 'required|in:male,female',
            'country' => 'required|max:255'
        ];

        $this->validate($request->all(), $rules);

        $book = Book::create($request->all());
        
        return $this->successResponse($book, Response::HTTP_CREATED);
    }

    public function update(Request $request, $book){
        $rules = [
            'name' => 'required|max:255',
            'gender' => 'required|in:male,female',
            'country' => 'required|max:255'
        ];

        $this->validate($request->all(), $rules);

        $book = Book::findOrFail($book);

        $book->fill($request->all());

        if($book->isClean()){
            return $this->errorResponse('At least one value must be changed', Response::HTTP_UNPROCESSABLE_ENTITY);            
        }
        
        $book->save();

        return $this->successResponse($book);
    }

    public function destroy($book){
        $book = Book::findOrFail($book);
        $book->delete();
        return $this->successResponse($book);
    }
}
